----------------------------------------------------------
Presenting the Nougat Android Open Source Project Manifest
----------------------------------------------------------

## How to sync ##
[Repo](http://source.android.com/source/developing.html) is a tool provided by Google that
simplifies using [Git](http://git-scm.com/book) in the context of the Android source.

```bash
# Create a directory for the source files
# You can name this directory however you want, just remember to replace
# WORKSPACE with your directory for the rest of this guide.
# This can be located anywhere (as long as the fs is case-sensitive)
$ mkdir 7
$ cd 7

# Install Repo in the created directory
# Use a real name/email combination, if you intend to submit patches
$ repo init -u https://gitlab.com/NAOSP/platform_manifest -b n
```
### Downloading the source tree ###

This is what you will run each time you want to pull in upstream changes. Keep in mind that on your
first run, it is expected to take a while as it will download all the required Android source files
and their change histories.

```bash
# Let Repo take care of all the hard work
#
# The -j# option specifies the number of concurrent download threads to run.
# 4 threads is a good number for most internet connections.
# You may need to adjust this value if you have a particularly slow connection.
$ repo sync -c -f -j8 --force-sync --no-clone-bundle --no-tags
```

#### Syncing specific projects ####

In case you are not interested in syncing all the projects, you can specify what projects you do
want to sync. This can help if, for example, you want to make a quick change and quickly push it
back for review. You should note that this can sometimes cause issues when building if there is
a large change that spans across multiple projects.

```bash
# Specify one or more projects by either name or path

# For example, enter NAOSP/android_frameworks_base or
# frameworks/base to sync the frameworks/base repository

$ repo sync PROJECT
```



